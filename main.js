// #ifndef VUE3
import Vue from 'vue'
import App from './App'
import store from '@/store/store.js'
import { createApp } from 'vue';
import App from './App.vue';
import { createPinia } from 'pinia';

const app = createApp(App);

// 注册 Pinia 插件
const pinia = createPinia();
app.use(pinia);

app.mount('#app');
import Vant from 'vant';
import 'vant/lib/index.css';
app.use(Vant);
Vue.config.productionTip = false
Vue.prototype.$eventBus = new Vue()


// const baseUrl=''
// Vue.prototype.$axios = axios;

App.mpType = 'app'

const app = new Vue({
	store,
	...App
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
import App from './App.vue'
import Vant from 'vant';
import 'vant/lib/index.css';
export function createApp() {
	const app = createSSRApp(App)
	app.use(Vant);
	return {
		app
	}
}

// #endif