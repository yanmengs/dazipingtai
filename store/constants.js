// 短token字段
export const ACCESS_TOKEN = 'a_tk'
// 长token字段
export const REFRESH_TOKEN = 'r_tk'
// header头部 携带短token
export const AUTH = 'Authorization'
// header头部 携带长token
export const PASS = 'pass'
//基本的接口地址
export const api = 'https://meet.ysyxmy.top'
// export const api = 'http://16.tcp.cpolar.top:11995'
//获取屏幕高度来计算
export const pk='MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCxrPhaBDwwFXbuNF4BPNcO0gk657zDQK7feYSLUDZzqJLNLocu0kMW8oCxN/sazFEJUkTzIW1J18MCl/dpTrevQlVdTeW9JYlkxtheaHxtBpqSbAd0f4u95Hgr9NY6dYHAVaAJ4BALNN41NXeeiS+LyIgNz2pnbVjVOf6OyeCvlQIDAQAB'
export const gettime = (timestamps) => {
	const dat = new Date(timestamps);

	// 使用 Date 对象的方法获取具体的日期和时间信息
	const year = dat.getFullYear();
	const month = dat.getMonth() + 1; // 月份从 0 开始，需要加 1
	const day = dat.getDate();
	const hours = dat.getHours();
	const minutes = dat.getMinutes();
	const seconds = dat.getSeconds();

	// 将获取到的信息拼接成你想要的格式
	const formattedDateTime = `${year}年${month}月${day}日 ${hours}时${minutes}分${seconds}秒`;
	return formattedDateTime
}

export const truncateString = (inputString) => {
	
	var imgIndex = inputString.indexOf('**/img/**');

	// 如果找到了 /img/
	if (imgIndex !== -1) {
		// 返回 /img/ 前面的子字符串
		
		return inputString.substring(0, imgIndex).substring(0, 20).split('*')[0]+ "...";
	} else {
		// 如果没有找到 /img/
		// 截取前20个字符并加上省略号+ "..."
		if (inputString.length > 20) {
			return inputString.substring(0, 20).split('*')[0] + "...";
		} else {
			return inputString;
		}
	}
}