import * as constants from '@/store/constants.js'
import JSEncrypt from 'jsencrypt';
import CryptoJS from 'crypto-js';
import {
	TextEncoder,
	TextDecoder
} from 'text-encoding';
// 存储短token
export const setAccessToken = (token) => uni.setStorage({
	data: token,
	key: constants.ACCESS_TOKEN
})
// 存储长token
export const setRefershToken = (token) => uni.setStorage({
	data: token,
	key: constants.REFRESH_TOKEN
})
// 获取短token
export const getAccessToken = () => uni.getStorageSync('a_tk');
// 获取长token
export const getRefershToken = () => uni.getStorageSync('r_tk');
// 删除短token
export const removeAccessToken = () => uni.removeStorage({
	key: constants.ACCESS_TOKEN
})
// 删除长token
export const removeRefershToken = () => uni.removeStorage({
	key: constants.REFRESH_TOKEN
})

//关于拦截器部分

// 刷新 Token 的函数
export const refreshToken = () => {
	return new Promise((resolve, reject) => {
		// 发送刷新 Token 的请求
		const value = uni.getStorageSync('r_tk');
		uni.request({
			url: '/auth/refresh',
			method: 'POST',
			header: {
				refresh_token: value,
			},
			success(res) {
				console.log(res)
				//清除缓存
				removeAccessToken()
				removeRefershToken()
				//存储双token
				setAccessToken(res.data.data.access_token)
				setRefershToken(res.data.data.refresh_token)
			}
		})
	})
}
uni.addInterceptor('request', {
	invoke(args) {
		// request 触发前拼接 url 
		args.url = 'https://meet.ysyxmy.top' + args.url
	},
	success(args) {
		console.log(args.data.code)
		if (args.data.code === 402) {
			const currentRequest = uni.getStorageSync('currentRequest');
			return refreshToken().then((res) => {
				return new Promise((resolve, reject) => {
					uni.request({
						url: currentRequest.url,
						method: currentRequest.method,
						data: currentRequest.data,
						success(res) {
							resolve(res)
						},
						fail(err) {
							reject(err)
						}
					});
				});
				return uni.request(res.config)
			})
		}
		if (args.data.code === 406 || args.data.code === 401) {
			// 清除缓存重新登录
			removeAccessToken()
			removeRefershToken()
			uni.redirectTo({
				url: '/pages/Login/Login'
			})
		}
	},
	fail(err) {
		console.log('interceptor-fail', err)
	},
	complete(res) {}
})
//获取公钥
export const getKey = () => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: '/auth/manage/getKey',
			method: 'POST',
			success: (res) => {
				console.log('密钥')
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			},
		})
	})
}
// 登录接口
export const login = (email, password) => {
	let t = ''
	if (uni.getStorageSync('detail')) {
		t = uni.getStorageSync('detail')
	}
	uni.clearStorageSync()
	if (t)
		uni.setStorageSync('detail', t)
	getKey().then(res => {
		console.log(res)
		console.log(password)
		var jsencrypt = new JSEncrypt()
		jsencrypt.setPublicKey(res.data.data.publicKey)
		var id = jsencrypt.encrypt(email)
		var passWord = jsencrypt.encrypt(password)
		const data = {
			email: id,
			password: passWord,
			publicKey: res.data.data.publicKey
		}
		uni.request({
			url: '/auth/passwordLogin',
			method: 'POST',
			data: data,
			success: (res) => {
				console.log(res)
				if (res.data.code != '200') {
					uni.showToast({
						title: '账号密码错误',
						icon: 'fail',
						duration: 1500
					})
					return
				}
				uni.setStorageSync('userid', res.data.data.userId)
				setAccessToken(res.data.data.access_token)
				setRefershToken(res.data.data.refresh_token)
				uni.showToast({
					title: '登录成功',
					icon: 'success',
					duration: 1500
				})
				setTimeout(() => {
					uni.switchTab({
						url: '/pages/Home/Home'
					})
				})
			},
			fail: (err) => {
				console.log(err)
				uni.showToast({
					title: '登录错误',
					icon: 'error',
					duration: 1500
				})
			}
		})
	})
}
//注册接口
export const regin = (username, email, password, code) => {
	return new Promise((resolve, rej) => {
		getKey().then(res => {
			console.log(res)
			var jsencrypt = new JSEncrypt()
			jsencrypt.setPublicKey(res.data.data.publicKey)
			console.log(username)
			var name = jsencrypt.encrypt(username)
			var id = jsencrypt.encrypt(email)
			var passWord = jsencrypt.encrypt(password)
			const data = {
				userName: name,
				email: id,
				password: passWord,
				code: code,
				publicKey: res.data.data.publicKey
			}

			uni.request({
				url: '/user/register',
				method: 'POST',
				data: data,
				success: (res) => {
					console.log(res)
					if (res.data.code == 200) {
						uni.showToast({
							title: res.data.msg,
							icon: 'success',
							duration: 1500
						})
					} else {
						uni.showToast({
							title: res.data.msg,
							icon: 'fail',
							duration: 1500
						})
					}

					resolve(res.data.msg)
				},
				fail: (err) => {
					console.log(err)
					uni.showToast({
						title: err.data.msg,
						icon: 'error',
						duration: 1500
					})
					rej(err.data.msg)
				},

			})
		})
	})

}
//退出登录接口
export const logoff = () => {
	const value = uni.getStorageSync('a_tk');
	uni.request({
		url: '/auth/logout',
		method: 'POST',
		header: {
			access_token: value
		},
		success(res) {
			uni.$emit('closeWs', true)
			let t = ''
			if (uni.getStorageSync('detail')) {
				t = uni.getStorageSync('detail')
			}
			uni.clearStorageSync()
			if (t)
				uni.setStorageSync('detail', t)
			uni.closeSocket()
			uni.reLaunch({
				url: '/pages/Login/Login'
			});
		}
	})
}
//获取轮播图接口
export const getSwiper = () => {
	const value = uni.getStorageSync('a_tk');
	return new Promise((resolve, reject) => {
		uni.request({
			url: '/static/rotatingPic',
			method: 'GET',
			header: {
				access_token: value
			},
			success(res) {
				resolve(res.data); // 将接口返回的数据传递给调用方
			},
			fail(err) {
				reject(err); // 将错误信息传递给调用方
			}
		});
	})
}
//获取个人信息接口
export const getuserInfo = () => {
	const value = uni.getStorageSync('a_tk');

	return new Promise((resolve, reject) => {
		uni.request({
			url: '/user/userInfo',
			method: 'GET',
			header: {
				access_token: value
			},
			success(res) {
				resolve(res.data); // 将接口返回的数据传递给调用方
			},
			fail(err) {
				reject(err); // 将错误信息传递给调用方
			}
		});
	});
}
// 获取其它用户信息
export const getUserinfo = (data) => {
	const value = uni.getStorageSync('a_tk')
	uni.request({
		url: '/user/userInfoById',
		method: 'GET',
		data: data,
		success(res) {
			console.log(res)
		}
	})
}
//修改个人信息
export const putuserInfo = (data) => {
	return new Promise((resolve, reject) => {
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: '/user/userInfo',
			method: 'PUT',
			header: {
				access_token: value
			},
			data: data,
			success(res) {

				resolve(res.data); // 使用 Promise 的 resolve 方法将成功获取到的用户信息数据传递出去
			},
			fail(err) {
				reject(err); // 如果请求失败，使用 Promise 的 reject 方法将错误传递出去
			}
		})
	})
}
//用于上传图片获取url的接口
export const uploadimg = (url) => {
	return new Promise((resolve, reject) => {
		uni.uploadFile({
			url: 'http://8.146.208.139:4000/upload', // 替换成你的服务器上传图片的API地址
			filePath: url,
			name: 'img',
			success: (uploadRes) => {
				// console.log(uploadRes.data)
				resolve(uploadRes.data)
				// if (res.data.code === 200) {
				// 	console.log(res.data);
				// 	resolve(res.data); // 使用 Promise 的 resolve 方法将成功获取到的用户信息数据传递出去
				// }
			},
			fail: (err) => {
				console.log(err)
				uni.showToast({
					title: '图片获取失败！',
					icon: 'none',
					duration: 2000
				})
			}
		})
	})
}
//发布帖子接口
export const pushpost = (data) => {
	return new Promise((resolve, reject) => {
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: '/post/edit',
			method: 'POST',
			header: {
				access_token: value
			},
			data: data,
			success(res) {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
//请求帖子分页接口
export const getpost = (pageNum, pageSize, tagId) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: '/post/list',
			method: 'GET',
			data: {
				pageNum: this.pageNum,
				pageSize: this.pageSize,
				tagId: this.tagId
			},
			success(res) {
				console.log(res)
			},
			fail(err) {
				reject(err)
			}
		})
	})
}
// 展示帖子接口
export const showpost = (ueseid) => {
	return new Promise((resolve, reject) => {
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: `/post/list/${ueseid}`,
			header: {
				access_token: value
			},
			success: (res) => {
				// console.log(res)
				resolve(res.data)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})

}
//罗列帖子
export const showAllpost = (data) => {
	return new Promise((resolve, reject) => {
		let jsonData = JSON.stringify(data);
		console.log(jsonData)
		uni.request({
			url: '/post/list',
			data: jsonData,
			method: 'POST',
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
//罗列帖子所匹配的人
export const matchPoster = (mesid) => {
	return new Promise((resolve, reject) => {
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: `/post/matched?postId=${mesid}`,
			method: 'GET',
			header: {
				access_token: value
			},
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
//用户取消匹配
export const deleatUser = (id) => {
	return new Promise((resolve, reject) => {
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: `/post/matched/cancel/${id}`,
			header: {
				access_token: value
			},
			method: 'DELETE',
			success: (res) => {
				console.log(res)
				resolve(res)
				uni.showToast({
					title: '删除成功',
					icon: 'success',
					duration: 1000
				})
			},
			fail: (err) => {
				console.log(err)
				uni.showToast({
					title: '删除失败',
					icon: 'fail',
					duration: 1000
				})
			}
		})
	})
}
// 罗列用户本人发送的帖子接口
export const getMypost = (pageNum, pageSize) => {
	return new Promise((resolve, reject) => {
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: '/post/getme',
			method: 'GET',
			header: {
				access_token: value
			},
			data: {
				pageNum: pageNum,
				pageSize: pageSize
			},
			success(res) {
				// console.log(res)
				resolve(JSON.stringify(res.data))
			},
			fail(err) {
				reject(err)
			}
		})
	})
}
// 修改用户本人发送的帖子
export const alterMypost = (data) => {
	return new Promise((resolve, reject) => {
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: '/post/getme',
			method: 'PUT',
			header: {
				access_token: value
			},
			data: data,
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})

}
// 删除用户本人发送的帖子
export const deleatpost = (id) => {
	return new Promise((resolve, reject) => {
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: `/post/getme/${id}`,
			method: 'DELETE',
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				console.log(err)
				reject(err)
			}
		})
	})
}
//获取用户本人的匹配历史
export const getMymatch = (data) => {
	return new Promise((resolve, reject) => {
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: '/post/getme/matched',
			method: 'GET',
			data: data,
			header: {
				access_token: value
			},
			success(res) {
				console.log(res)
				resolve(res)
			},
			fail(err) {
				console.log(err)
				reject(err)
			}
		})
	})
}
// 收藏帖子
export const addMyfavoritepost = (id) => {
	const value = uni.getStorageSync('a_tk');
	let data = {
		id: id
	}
	uni.request({
		url: '/post/favorite',
		method: 'POST',
		header: {
			access_token: value
		},
		data: data,
		success: (res) => {
			console.log(res)
		},
		fail: (err) => {
			console.log(err)
		}
	})
}
// 移除收藏的帖子
export const removeMyfavoritepost = (id) => {
	const value = uni.getStorageSync('a_tk');
	uni.request({
		url: '/post/favorite',
		method: 'DELETE',
		header: {
			access_token: value
		},
		data: {
			id: id
		},
		success: (res) => {
			console.log(res)
		},
		fail: (err) => {
			console.log(err)
		}
	})
}
// 罗列收藏的帖子
export const getMyfavoritepost = (pageNum, pageSize) => {
	return new Promise((resolve, reject) => {
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: '/post/favorite',
			method: 'GET',
			data: {
				pageNum: pageNum,
				pageSize: pageSize
			},
			header: {
				access_token: value
			},
			success(res) {
				// console.log(res)
				resolve(res)
			},
			fail(err) {
				reject(err)
			}
		})
	})
}
// 修改帖子状态 状态(0待匹配，1匹配完成，2挂起)"
export const alterpoststatue = (postId, status) => {
	const value = uni.getStorageSync('a_tk');
	uni.request({
		url: '/post/modify',
		method: 'POST',
		header: {
			access_token: value
		},
		data: {
			postId: this.postId,
			status: this.status
		},
		success(res) {
			console.log(res)
		}
	})
}
//用于与讯飞对话
export const chatToAi = (data) => {
	return new Promise((resolve, reject) => {
		const dat = {
			question: data
		}
		console.log(dat)
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: '/chat/gpt',
			method: 'POST',
			data: dat,
			header: {
				access_token: value
			},
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				console.log(err)
			}
		})
	})
}
//用于获取大约两行的内容
export const getcontent = async (userid) => {
	const value = uni.getStorageSync('a_tk');
	let count = 0;
	let index = 0;
	try {
		const res = await new Promise((resolve, reject) => {
			uni.request({
				url: `/post/list/${userid}`,
				success: (res) => {
					resolve(res);
				},
				fail: (err) => {
					reject(err);
				}
			});
		});
		console.log('这是获取前2行')
		console.log(res.data)
		for (let i = 0; i < Math.min(res.data.data.content.length, 20); i++) {
			if (str[i] === '*') {
				count++;
			} else {
				count = 0;
			}
			if (count < 2) {
				index = i;
			}
		}
		return str.substring(0, index + 1);
	} catch (err) {
		console.log(err);
		return '';
	}
}
//用于获取评论
export const getcommonents = (id, Num, Size) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: `/comment/list?postId=${id}&pageNum=${Num}&pageSize=${Size}`,
			method: 'POST',
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
//用于发表评论
export const postcomment = (data) => {
	return new Promise((resolve, reject) => {
		console.log(data)
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: '/comment/add',
			data: data,
			header: {
				access_token: value
			},
			method: 'POST',
			success: (res) => {
				console.log(res)
				resolve(res)
			},
			fail: (err) => {
				console.log(err)
			}
		})
	})

}
//用于获得子评论
export const getSoncommonents = (id, Num, Size) => {
	return new Promise((resolve, reject) => {
		// let data = {
		// 	commentId: id,
		// 	pageNum: Num,
		// 	pageSize: Size
		// }
		// console.log(data)
		uni.request({
			url: `/comment/get?commentId=${id}&pageNum=${Num}&pageSize=${Size}`,
			method: 'POST',
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
//用于删除本人的评论
export const deleteMycomment = (id) => {
	const value = uni.getStorageSync('a_tk');
	let data = {
		commentId: id
	}
	uni.request({
		url: '/comment/delete',
		data: data,
		header: {
			access_token: value
		},
		method: 'POST',
		success: (res) => {
			uni.showToast({
				title: '删除成功！',
				//将值设置为 success 或者直接不用写icon这个参数
				icon: 'success',
				//显示持续时间为 2秒
				duration: 2000
			})
			console.log(res)
		},
		fail: (err) => {
			uni.showToast({
				title: '删除失败！',
				//将值设置为 success 或者直接不用写icon这个参数
				icon: 'fail',
				//显示持续时间为 2秒
				duration: 2000
			})
			console.log(err)
		}
	})
}
//获取其他用户信息
export const getothermessage = (userid) => {
	return new Promise((resolve, reject) => {
		const value = uni.getStorageSync('a_tk');
		console.log(userid)
		let data = {
			userId: userid
		}
		uni.request({
			url: '/user/userInfoById',
			header: {
				access_token: value
			},
			data: data,
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
//用于获取聊天记录
export const getHistory = (data) => {
	return new Promise((resolve, reject) => {
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: '/chat/history',
			header: {
				access_token: value
			},
			data: data,
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
//获取有过聊天记录的人
export const getchatHistroyUser = () => {
	return new Promise((resolve, reject) => {
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: '/chat/history/user',
			header: {
				access_token: value
			},
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
//获取tags栏
export const getTags = () => {
	return new Promise((resolve, reject) => {
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: '/tag/list',
			header: {
				access_token: value
			},
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
//获取推荐列表
export const getreclist = (count) => {
	return new Promise((resolve, reject) => {
		const value = uni.getStorageSync('a_tk');
		uni.request({
			url: `/post/recommend?count=${count}`,
			header: {
				access_token: value
			},
			method: 'GET',
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
//获取注册邮箱验证码
export const getRegisteremailcode = (data) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: `/user/sendRegisterCodeToEmail`,
			method: 'POST',
			data: data,
			success: (res) => {
				console.log(res)
				if (res.data.code == 200) {
					uni.showToast({
						title: '验证码已发送',
						icon: 'success',
						duration: 1500
					})
				} else {
					uni.showToast({
						title: '发送失败',
						icon: 'fail',
						duration: 1500
					})
				}
				resolve(res.data.msg)
			},
			fail: (err) => {
				console.log(err)
				uni.showToast({
					title: '发送失败',
					icon: 'error',
					duration: 1500
				})
				rej(err.data.msg)
			},

		})
	})
}
//获取修改密码邮箱验证码
export const getFindBackemailcode = (data) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: `/user/sendPWResetCodeToEmail`,
			method: 'POST',
			data: data,
			success: (res) => {
				console.log(res)
				if (res.data.code == 200) {
					uni.showToast({
						title: '验证码已发送',
						icon: 'success',
						duration: 1500
					})
				} else {
					uni.showToast({
						title: '发送失败',
						icon: 'fail',
						duration: 1500
					})
				}
				resolve(res.data.msg)
			},
			fail: (err) => {
				console.log(err)
				uni.showToast({
					title: '发送失败',
					icon: 'error',
					duration: 1500
				})
				reject(err.data.msg)
			},

		})
	})
}
//验证是否已经注册过
export const verfryEmail = (email) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: `/user/checkEmailExist?email=${email}`,
			success: (res) => {
				console.log(res)

				if (res.data.data.isExist) {
					resolve(false)
				} else {

					resolve(true)

				}

			},
			fail: (err) => {
				console.log(err)

				reject(reject)
			}
		})
	})
}
//忘记密码重改
export const forgetPassword = (password, email, code) => {
	return new Promise((resolve, reject) => {
		getKey().then(res => {
			var jsencrypt = new JSEncrypt()
			jsencrypt.setPublicKey(res.data.data.publicKey)
			var id = jsencrypt.encrypt(email)
			var passWord = jsencrypt.encrypt(password)
			const data = {
				email: id,
				newPassword: passWord,
				code: code,
				publicKey: res.data.data.publicKey
			}
			uni.request({
				url: '/user/forgetPassword',
				data: data,
				method: 'POST',
				success: (res) => {
					console.log(res)
					if (res.data.code == 200) {
						uni.showToast({
							title: '修改成功',
							icon: 'success',
							duration: 1000
						})
					} else {
						uni.showToast({
							title: '修改失败',
							icon: 'fail',
							duration: 1000
						})
					}
				},
				fail: (err) => {
					uni.showToast({
						title: '修改失败',
						icon: 'fail',
						duration: 1000
					})
					reject(reject)
				}
			})
		})
	})
}
//创建社群
export const createCommunity = (data) => {
	return new Promise((resolve, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: '/group/create',
			header: {
				access_token: token
			},
			data: data,
			method: 'POST',
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
//罗列社群种类
export const getCategoryType = () => {
	return new Promise((resolve, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: `/category/list?pageNum=${1}&&pageSize=${20}`,
			header: {
				access_token: token
			},
			method: 'GET',
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
// 获取社群指定信息
export const communityDeatil = (id) => {
	return new Promise((resolve, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: `/group/detail?groupId=${id}`,
			method: 'GET',
			header: {
				access_token: token
			},
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
// 修改社群指定信息
export const modifyDetail = (data) => {
	return new Promise((resolve, reject) => {
		const token = uni.getStorageSync('a_tk');
		console.log(data)
		uni.request({
			url: '/group/detail/modify',
			method: 'PUT',
			header: {
				access_token: token
			},
			data: data,
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
// 罗列社群
export const getCommunityList = (id, query) => {
	return new Promise((resolve, reject) => {
		let data = null
		if (query)
			data = {
				pageNum: 1,
				pageSize: 20,
				categoryId: id,
				query: query
			}
		else
			data = {
				pageNum: 1,
				pageSize: 20,
				categoryId: id
			}
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: '/group/list',
			method: 'POST',
			header: {
				access_token: token
			},
			data: data,
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}



// 社群发布动态
export const postLive = (data) => {
	return new Promise((resolve, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: '/post/edit/live',
			method: 'POST',
			header: {
				access_token: token
			},
			data: data,
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}

// 社群打卡排名
export const getRank = (data) => {
	return new Promise((resolve, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: '/group/team/rank',
			method: 'GET',
			header: {
				access_token: token
			},
			data: data,
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}

// 获取社群队伍消息
export const teamList = (data) => {
	return new Promise((resolve, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: '/team/list',
			method: 'GET',
			header: {
				access_token: token
			},
			data: data,
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
//分享加密
export const encryptString = (text) => {
	// const cipher = crypto.createCipher('aes-256-cbc', 'Meet_RYDM');
	// let encrypted = cipher.update(text, 'utf8', 'hex');
	// encrypted += cipher.final('hex');
	// return '分享到Meet' + encrypted

	const encodedWord = new TextEncoder().encode(text);
	const base64String = btoa(String.fromCharCode(...new Uint8Array(encodedWord)));
	return '分享到Meet' + base64String;
}
//分享解密
export const decryptString = (ciphertext) => {
	// let t = constants.pk
	// const jsencrypt = new JSEncrypt();
	// jsencrypt.setPrivateKey(t);
	// return jsencrypt.decrypt(ciphertext.split('分享到Meet')[1]);

	const binaryString = atob(ciphertext.split('分享到Meet')[1]);
	const binaryLen = binaryString.length;
	const bytes = new Uint8Array(binaryLen);
	for (let i = 0; i < binaryLen; i++) {
		bytes[i] = binaryString.charCodeAt(i);
	}
	const decodedText = new TextDecoder().decode(bytes);
	return decodedText;
}

//获取社群动态列表
export const getCommuntyPost = (data) => {
	return new Promise((resolver, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: '/post/list/live',
			method: 'POST',
			data: data,
			header: {
				access_token: token
			}
		}).then(res => {
			resolver(res)
		}).catch(err => {
			reject(err)
		})
	})
}

//展示动态
export const showCommuntyPost = (id) => {
	return new Promise((resolver, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: `/post/list/live/${id}`,
			method: 'POST',
			header: {
				access_token: token
			}
		}).then(res => {
			resolver(res)
		}).catch(err => {
			reject(err)
		})
	})
}

//退出社群
export const quitCommunty = (id) => {
	return new Promise((resolver, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: `/group/quit?groupId=${id}`,
			method: 'DELETE',
			header: {
				access_token: token
			}
		}).then(res => {
			resolver(res)
		}).catch(err => {
			reject(err)
		})
	})
}


//获取社群中总队伍打卡排名(得到排名前十)
export const getTeamRank = (id) => {
	return new Promise((resolver, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: `/group/team/rank?groupId=${id}`,
			header: {
				access_token: token
			}
		}).then(res => {
			resolver(res)
		}).catch(err => {
			reject(err)
		})
	})
}

//获取我的社群
export const getMyCommunty = (pageNum, pageSize) => {
	return new Promise((resolver, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: `/group/getme?pageNum=${pageNum}&pageSize=${pageSize}`,
			header: {
				access_token: token
			}
		}).then(res => {
			resolver(res)
		}).catch(err => {
			reject(err)
		})
	})
}
//发布匹配帖
export const publicMatch = (data) => {
	return new Promise((resolver, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: `/post/group/edit`,
			header: {
				access_token: token
			},
			method: 'POST',
			data: data
		}).then(res => {
			resolver(res)
		}).catch(err => {
			reject(err)
		})
	})
}

//获取所有队伍
export const getAllTeam = (id) => {
	return new Promise((resolver, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: `/group/team/list?groupId=${id}`,
			header: {
				access_token: token
			}
		}).then(res => {
			resolver(res)
		}).catch(err => {
			reject(err)
		})
	})
}

//获取社群中我加入的队伍
export const MyTeam = (size, num) => {
	return new Promise((resolver, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: `/team/getme?pageSize=${num}&pageNum=${size}`,
			header: {
				access_token: token
			}
		}).then(res => {
			resolver(res)
		}).catch(err => {
			reject(err)
		})
	})
}

//获取社群中和队伍成员信息
export const TeamWithMe = (id) => {
	return new Promise((resolver, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: `/team/member/list?teamId=${id}`,
			header: {
				access_token: token
			}
		}).then(res => {
			resolver(res)
		}).catch(err => {
			reject(err)
		})
	})
}

//修改队伍信息
export const changeAboutTeam = (data) => {
	return new Promise((resolver, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: `/team/detail/modify`,
			header: {
				access_token: token
			},
			data: data,
			method: 'PUT'
		}).then(res => {
			resolver(res)
		}).catch(err => {
			reject(err)
		})
	})
}

//发起加入申请
export const applyJoin = (id) => {
	return new Promise((resolver, reject) => {
		let data = {
			groupId: id
		}
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: `/email/applyGroup`,
			header: {
				access_token: token
			},
			data: data,
			method: 'POST'
		}).then(res => {
			resolver(res)
		}).catch(err => {
			reject(err)
		})
	})
}


//申请处理
export const rebackJoin = (data) => {
	return new Promise((resolver, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: `/email/applyGroup/handle`,
			header: {
				access_token: token
			},
			data: data,
			method: 'POST'
		}).then(res => {
			resolver(res)
		}).catch(err => {
			reject(err)
		})
	})
}

//获取信息列表
export const MessageList = (num, size) => {
	return new Promise((resolver, reject) => {
		const token = uni.getStorageSync('a_tk');
		uni.request({
			url: `/email/sys/getme?pageNum=${num}&pageSize=${size}`,
			header: {
				access_token: token
			}
		}).then(res => {
			resolver(res)
		}).catch(err => {
			reject(err)
		})
	})
}